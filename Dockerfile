## Build Image
FROM debian:buster AS compile-image

MAINTAINER John Kristensen <john@jerrykan.com>

RUN apt-get update \
    && apt-get upgrade -y \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        git \
        python3 \
        python3-venv \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# See: https://pythonspeed.com/articles/activate-virtualenv-dockerfile/
ENV VIRTUAL_ENV="/app/venv"
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

WORKDIR /app

COPY requirements.txt /app/requirements.txt

RUN python3 -m venv "$VIRTUAL_ENV" \
    && pip install -r requirements.txt

COPY skills/ /app/skills/
COPY configuration.yaml /app/configuration.yaml


## Runtime Image
FROM debian:buster

RUN set -x \
    && apt-get update \
    && apt-get upgrade -y \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        python3 \
        python3-venv \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && useradd --create-home chatbot

WORKDIR /app

COPY --from=compile-image --chown=chatbot:chatbot /app /app

USER chatbot

CMD [ "/app/venv/bin/opsdroid", "start" ]
