# TuzBot
## Install
    $ python3 -m venv venv
    $ source venv/bin/activate
    $ pip install -r requirements.txt

## Run
    $ export TUZBOT_PASSWORD="xxxx"
    $ opsdroid


## Docker
### Build Image
A new docker image can be built by running the following command:

    $ docker build . -t taslug/chatbot

### Use Image
The docker image can be used to run the chatbot with the following command:

    $ docker run -ti --env TUZBOT_PASSWORD=<password> taslug/chatbot
