import re

from opsdroid import events
from opsdroid.matchers import match_event
from opsdroid.skill import Skill


class SetPermLevelsSkill(Skill):
    def __init__(self, opsdroid, config):
        super().__init__(opsdroid, config)

        self._connector = opsdroid.default_connector

        self._room_id = self._connector.room_ids[config['room']]
        self._user_regex = config['user_regex']
        self._power_level = config['power_level']

    @match_event(events.OpsdroidStarted)
    async def startup(self, event):
        members = await self._connector.connection.get_room_members(
            self._room_id)
        roles = await self._connector.connection.get_power_levels(
            self._room_id)

        members = [
            m['state_key']
            for m in members.get('chunk', [])
            if m.get('content', {}).get('membership') == 'join'
        ]
        members = [
            m for m in members if
            re.fullmatch(self._user_regex, m)
        ]

        for member in members:
            if roles['users'].get(member, 0) < self._power_level:
                roles['users'][member] = self._power_level

        await self._connector.connection.set_power_levels(self._room_id, roles)

    @match_event(events.JoinRoom)
    async def user_join(self, event):
        if event.target != self._room_id:
            return

        if not re.fullmatch(self._user_regex, event.user_id):
            return

        roles = await event.connector.connection.get_power_levels(event.target)

        if roles['users'].get(event.user_id, 0) < self._power_level:
            await event.respond(events.UserRole(self._power_level))
