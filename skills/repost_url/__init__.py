import logging
from socket import gaierror, getaddrinfo

from opsdroid.events import Message
from opsdroid.skill import Skill
from opsdroid.matchers import match_regex

_LOGGER = logging.getLogger(__name__)

label_re = r'[a-zA-Z0-9][-a-zA-Z-0-9]{0,61}[a-zA-Z0-9]'
domain_re = r'({label}\.)+{label}'.format(label=label_re)


class RepostUrlSkill(Skill):
    @match_regex(domain_re, matching_condition='search')
    async def repost_url(self, message):
        room_by_id = {
            rid: name for name, rid in
            message.connector.room_ids.items()
        }
        target = room_by_id.get(message.target)
        target_room = self.config.get('repost_rooms', {}).get(target)

        if target_room is None:
            return

        _LOGGER.debug("Matched domain '%s'", message.regex.group(0))

        try:
            getaddrinfo(message.regex.group(0), None)
        except gaierror:
            return

        await message.respond(Message(
            '{}: {}'.format(message.user, message.text),
            target=target_room,
        ))
